# bi0s blog

[blog.bi0s.in](https://blog.bi0s.in) proudly published by [teambi0s](https://bi0s.in)

## How to Build locally

To set up and work with this project locally, follow the steps below:

1. Fork, clone or download this project
2. Install [Hexo](https://hexo.io/docs/)
3. Install all dependencies with `npm install`
4. Run `python getPost.py` to generate your writeup README.

![](documentation/demo.gif)

5. Run the preview locally: `hexo server`
    + Use `-p <port>` to run on a different port than default `4000`
6. Send over a pull request!

Read more at [Hexo docs](https://hexo.io/docs/)

## Sample Format for writing your blog post

```
---
title: Proper Title
date: 2019-10-14 17:09:06
author: user
author_url: https://twitter.com/user
categories:
  - Crypto
tags:
  - <CTF Name>
  - Writeup
  - Crypto
---

One line content description (Optional)

**tl;dr**

+ Something
+ Something

<!--more-->

**Challenge points**: 
**No. of solves**: 
**Solved by**: [username](https://twitter.com/username) <!--Change username-->

## Challenge Description

    -----the rest----
```
+ Title should be something similar to this: `Challenge Name - CTF Name Year`.

Eg: `Lookout Foxy - InCTF Internationals 2020`

## General guidelines:

* Add a New Line after `tl;dr` and before `<!--more-->`. Else there will be homepage UI issues.

* Always use `<!--more-->` to split the post with excerpt
  + See https://gitlab.com/teambi0s/blog/raw/master/source/_posts/Crypto/Digital-Signatures/midnightsun-quals19-ezdsa.md for reference

* Use footnotes with [^1]
    * More Syntax [here](https://github.com/LouisBarranqueiro/hexo-footnotes)

* Slide HTML in front-matter, (use any one of the folowing)
    * slidehtml: true
    * slidehtml:
        titleMerge: true
        verticalSeparator: \n--\n
    * slidehtml:
        titleMerge: true

* Never encrypt your blog

* Use author2 and author2-url for 2nd author (in the front-matter)
  + For example:
    + author2: spyd3r
    + author2-url: https://twitter.com/TarunkantG

* To add latex equations to your post
  + Enable it in your post by adding `mathjax: true` in the [front-matter](https://hexo.io/docs/front-matter) of your post
  + You can now add latex equations by enclosing them in `$`. For example: `$ a + b = c $`
